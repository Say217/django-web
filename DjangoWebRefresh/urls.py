"""DjangoWebRefresh URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
import tdd.urls as tdd
from django.conf.urls import url, include
from tdd import views

app_name = "tdd"

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^tdd/', include(('tdd.urls', 'tdd'),namespace='tdd')),
    url(r'^$', views.status_form, name='status_form'),
]
