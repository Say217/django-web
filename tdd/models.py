from django.db import models

# Create your models here.
class StatusModel(models.Model):
	status = models.CharField(max_length=300)
	time = models.TimeField((u"Conversation Time"),auto_now_add=True, blank=True)