from django.conf.urls import url
from .views import status_form
    
#url for app
urlpatterns = [
    url(r'^$', status_form, name='status_form'),
]