from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from .views import status_form
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
import time

class TDDTest(TestCase):
    def test_tdd_url_is_exist(self):
        response = Client().get('/tdd/')
        self.assertEqual(response.status_code,200)

    def test_tdd_using_to_do_list_template(self):
        response = Client().get('/tdd/')
        self.assertTemplateUsed(response, 'hello.html')

    def test_tdd_using_index_func(self):
        found = resolve('/tdd/')
        self.assertEqual(found.func, status_form)

class FunctionalTest(LiveServerTestCase):
	def setUp(self):
		firefox_options = Options()
		firefox_options.add_argument('--headless')
		self.browser = webdriver.Firefox(firefox_options=firefox_options)

	def tearDown(self):
		self.browser.quit()

	def test_form_submit(self):
		self.browser.get(self.live_server_url)
		status_box = self.browser.find_element_by_name('status')
		status_box.send_keys('coba-coba')
		status_box.submit()
		time.sleep(1)
		self.assertIn("coba-coba", self.browser.page_source)

	def test_submit_color(self):
		self.browser.get(self.live_server_url)
		submit = self.browser.find_element_by_id('id_submit')
		self.assertEqual(submit.get_attribute('style'), 'background-color: white;')

	def test_row_color(self):
		self.browser.get(self.live_server_url)
		row = self.browser.find_element_by_id('id_row')
		self.assertEqual(row.get_attribute('style'), 'background-color: gray;')

	def test_header_position(self):
		self.browser.get(self.live_server_url)
		header = self.browser.find_element_by_id('id_header')
		self.assertEqual(header.get_attribute('class'), 'text-center')

	def test_status_title_position(self):
		self.browser.get(self.live_server_url)
		status_title = self.browser.find_element_by_id('id_stat_title')
		self.assertEqual(status_title.get_attribute('class'), '') #tidak saya atur
		self.assertEqual(status_title.get_attribute('style'), '') #tidak saya atur