from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import StatusForm
from .models import StatusModel

# Create your views here.

diary_dict = {}
# def index(request):
#     return render(request, 'hello.html', {'diary_dict' : diary_dict})

def status_form(request):
    form = StatusForm(request.POST or None)
    if request.method == "POST":
        if form.is_valid():
            form.save()
    status_list = StatusModel.objects.all()
    return render(request, "hello.html", {'form' : form, "status_list" : status_list})