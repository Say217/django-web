from django.apps import AppConfig


class TDDConfig(AppConfig):
    name = 'tdd'
