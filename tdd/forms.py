from django import forms
from .models import StatusModel

class StatusForm(forms.ModelForm):
    class Meta:
        model = StatusModel
        fields = "__all__"  # all of models are inserted to fields  